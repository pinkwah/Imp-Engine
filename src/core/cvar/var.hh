#ifndef __VAR__98822993
#define __VAR__98822993

#include <prelude.hh>
#include "data.hh"

#define IMP_DEFINE_ARITHMETIC_BOTH(_Type, _Op)                          \
    inline _Type operator _Op(const VarBase<_Type>& lhs, const VarBase<_Type>& rhs) \
    { return lhs.get() _Op rhs.get(); }

#define IMP_DEFINE_ARITHMETIC_LEFT(_Type, _Op)                          \
    inline _Type operator _Op(const VarBase<_Type>& lhs, const _Type& rhs) \
    { return lhs.get() _Op rhs; }

#define IMP_DEFINE_ARITHMETIC_RIGHT(_Type, _Op)                         \
    inline _Type operator _Op(const _Type& lhs, const VarBase<_Type>& rhs) \
    { return lhs _Op rhs.get(); }

#define IMP_DECLARE_ASSIGN_ARITHMETIC(_Op)              \
    VarBase& operator _Op ## =(const VarBase<T>& other) \
    { return *this = (m_value _Op other); }             \
        VarBase& operator _Op ## =(const T& other)      \
        { return *this = (m_value _Op other); }

#define IMP_DEFINE_ARITHMETIC(_Type, _Op)       \
    IMP_DEFINE_ARITHMETIC_BOTH(_Type, _Op)      \
        IMP_DEFINE_ARITHMETIC_LEFT(_Type, _Op)  \
        IMP_DEFINE_ARITHMETIC_RIGHT(_Type, _Op)

 namespace imp::cvar {
   template <class T>
   class RefBase;
   class Ref;
   class Store;

   template <class T>
   class VarBase {
       Arc<Data> m_data;
       T& m_value;

       friend class RefBase<T>;
       friend class Ref;
       friend class Store;

   public:
       using value_type = T;

       VarBase():
           m_data(std::make_shared<Data>(T {})),
           m_value(m_data->get<T>()) {}

       VarBase(const T& default_):
           m_data(std::make_shared<Data>(default_)),
           m_value(m_data->get<T>()) {}

       VarBase(T&& default_):
           m_data(std::make_shared<Data>(std::move(default_))),
           m_value(m_data->get<T>()) {}

       VarBase& operator=(const T& new_value)
       {
           m_value = new_value;
           m_data->update();
           return *this;
       }

       VarBase& operator=(T&& new_value)
       {
           m_value = std::move(new_value);
           m_data->update();
           return *this;
       }

       ~VarBase()
       { m_data->set_valid(false); }

       IMP_DECLARE_ASSIGN_ARITHMETIC(+);
       IMP_DECLARE_ASSIGN_ARITHMETIC(-);
       IMP_DECLARE_ASSIGN_ARITHMETIC(*);
       IMP_DECLARE_ASSIGN_ARITHMETIC(/);
       IMP_DECLARE_ASSIGN_ARITHMETIC(%);

       void set_to_default(bool inhibit_callback = false)
       {
           m_data->set_to_default();
           if (!inhibit_callback) {
               m_data->update();
           }
       }

       void set_callback(const std::function<void (const T&)>& f)
       { m_data->set_callback(f); }

       bool is_config() const
       { return m_data->test_flag(Flag::config); }

       operator const T&() const
       { return m_value; }

       const T& get() const
       { return m_value; }

       const T& get_default() const
       { return m_data->get_default<T>(); }
   };

   IMP_DEFINE_ARITHMETIC(int64, +);
   IMP_DEFINE_ARITHMETIC(int64, -);
   IMP_DEFINE_ARITHMETIC(int64, *);
   IMP_DEFINE_ARITHMETIC(int64, /);
   IMP_DEFINE_ARITHMETIC(int64, %);

   using IntVar = VarBase<int64>;
   using StrVar = VarBase<String>;
 }

#undef IMP_DEFINE_ARITHMETIC
#undef IMP_DECLARE_ASSIGN_ARITHMETIC
#undef IMP_DEFINE_ARITHMETIC_RIGHT
#undef IMP_DEFINE_ARITHMETIC_LEFT
#undef IMP_DEFINE_ARITHMETIC_BOTH

#endif //__VAR__98822993
