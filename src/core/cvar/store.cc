#include "ref.hh"
#include "store.hh"
#include <iostream>

using namespace imp;
namespace internal = imp::cvar::internal;

namespace {
  using Vars = HashMap<String, Weak<cvar::Data>>;

  String s_standard(StringView name)
  {
      String cname;

      for (auto c : name) {
          c = std::tolower(c);
          if ((c < 'a' || c > 'z') && (c < '0' || c > '9') && c != '_') {
              continue;
          }
          cname.push_back(c);
      }

      return cname;
  }

  template <class T>
  Optional<cvar::RefBase<T>> s_find(Vars& vars, const String& name)
  {
      auto it = vars.find(name);
      if (it == vars.end()) {
          return std::nullopt;
      }

      auto data = it->second.lock();
      if (!data) {
          vars.erase(it);
          return std::nullopt;
      }

      cvar::RefBase<T> ref { data };
      return ref;
  }
}

//
// global g_store
//
Box<cvar::Store> cvar::g_store = nullptr;

//
// private Store::p_add
//

void cvar::Store::p_add(Arc<Data> data, StringView name, StringView desc, const FlagSet& flags)
{
    String sname = s_standard(name);

    auto it = m_vars.find(sname);
    if (it != m_vars.end()) {
        throw cvar::var_already_exists(static_cast<String>(name));
    }

    data->set_name(name);
    data->set_desc(desc);
    data->set_valid(true);
    data->set_flags(flags);

    m_vars.emplace(sname, data);
}

//
// find
//

template <>
Optional<cvar::IntRef> cvar::Store::find<int64>(const String& name)
{
    return s_find<int64>(m_vars, s_standard(name));
}

//
// find_prefix
//


//
// internal find
//
template <>
Optional<cvar::IntRef> internal::find<int64>(StringView name)
{ return cvar::g_store->find<int64>(static_cast<String>(name)); }
