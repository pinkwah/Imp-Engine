#ifndef __PREDICATES__40898284
#define __PREDICATES__40898284

#include <prelude.hh>

namespace imp::cvar {
  class Data;

  /**! Dummy predicate that returns true for any input */
  class DummyPred {
  public:
      bool operator()(const String&, Weak<Data>&) const
      { return true; }
  };

  /**! Flag predicate that returns true if the given flag set is a subset of the
       cvar's */
  class FlagPred {
      FlagSet m_flags;

  public:
      FlagPred(const FlagSet& flags):
          m_flags(flags) {}

      FlagPred& operator=(const FlagPred&) = default;

      bool operator()(const String&, Weak<Data>& data) const
      {
          auto p = data.lock();
          return p && m_flags.is_subset_of(p->flags());
      }
  };

  /**! Predicate that returns true if the cvar's name starts with the given string */
  class PrefixPred {
      StringView m_prefix;

  public:
      PrefixPred(StringView prefix):
          m_prefix(prefix) {}

      bool operator()(const String& name, Weak<Data>&) const
      {
          StringView n = name;
          return n.substr(0, m_prefix.size()) == m_prefix;
      }
  };
}

#endif //__PREDICATES__40898284
