#ifndef __CMD__39908171
#define __CMD__39908171

#include <prelude.hh>
#include <boost/core/demangle.hpp>
#include <boost/lexical_cast.hpp>
#include <typeinfo>

namespace imp {
namespace cmd {

class CmdBase {
public:
    virtual ~CmdBase() {}

    virtual void call(ArrayView<String> args) = 0;

    virtual std::ostream& write(std::ostream& s) const = 0;
};

namespace internal {
template <class T>
struct type_name {
    static String name()
    {
        auto cstr = typeid(T).name();
        auto name = boost::core::demangle(cstr);

        return name;
    }
};

template <>
struct type_name<String> {
    static String name()
    { return "string"; }
};

template <>
struct type_name<StringView> {
    static String name()
    { return "string"; }
};
}

template <class... Args>
class Cmd : public CmdBase {
    using func_type = void (Args...);
    using tuple_type = Tuple<std::decay_t<Args>...>;

    func_type& m_func;

    template <class Array, size_t... I>
    void m_call(Array args, std::index_sequence<I...>)
    {
        tuple_type tuple {
            boost::lexical_cast<std::decay_t<Args>>(args[I])...
        };

        std::apply(m_func, tuple);
    }

public:
    Cmd(func_type& func):
        m_func(func) {}

    void call(ArrayView<String> args) override
    {
        if (args.size() != sizeof...(Args))
            return;

        m_call(args, std::index_sequence_for<Args...> {});
    }

    std::ostream& write(std::ostream& s) const override
    {
        std::array<String, sizeof...(Args)> tis {
            internal::type_name<std::decay_t<Args>>::name()...
        };

        if (tis.size() == 0) {
            return s << "()";
        }

        s << "(";
        for (size_t i {}; i < tis.size() - 1; ++i) {
            s << tis[i] << ", ";
        }
        return s << tis.back() << ")";
    }
};

template <class... Args>
std::ostream& operator<<(std::ostream& s, const CmdBase& cmd)
{ return cmd.write(s); }

class Store {
    HashMap<String, Arc<CmdBase>> m_cmds;

public:
    template <class... Args>
    void add(void (&func)(Args...), StringView name)
    { m_cmds[static_cast<String>(name)] = std::make_shared<Cmd<Args...>>(func); }

    Arc<CmdBase> find(const String& name) const
    {
        auto it = m_cmds.find(name);
        if (it == m_cmds.end())
            return nullptr;
        return it->second;
    }
};

extern Box<Store> g_store;

}
}

#endif //__CMD__39908171
