#include <initializer_list>
#include "program_options.hh"

//
// Program Options
//

void imp::program_options::parse_args(int argc, char **argv)
{
    using po = imp::program_options;
    po::argument_parser ap(argc, argv);

    ap.add_options({
            { "width", po::cvar_i64("v_Width"), "width" },
                { "w", po::alias() },

                    });
}

/**
 * Implementation of program options
 */

using priv = imp::program_options;

void priv::argument_parser::add_options(const std::initializer_list<option>& options)
{

}
