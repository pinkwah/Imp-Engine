#ifndef __IPLATFORM__41862208
#define __IPLATFORM__41862208

#include <filesystem>
#include <prelude.hh>

namespace imp {
  class IPlatform {
  public:
      virtual ~IPlatform() {}

      virtual void poll_events() = 0;

      virtual std::filesystem::path user_data_dir() = 0;
  };

  extern Box<IPlatform> g_platform;
}

#endif //__IPLATFORM__41862208
