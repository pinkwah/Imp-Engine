#ifndef __PRIVATE__26327343
#define __PRIVATE__26327343

#include <filesystem>

#include <core/cvar.hh>
#include <platform/iplatform.hh>
#include <platform/ivideo.hh>

/** Forward declare SDL2 stuff */
struct SDL_Window;
using SDL_GLContext = void*;

namespace imp::sdl2 {
  class Platform : public IPlatform {
  public:
      Platform();
      ~Platform() override;

      void poll_events() override;

      std::filesystem::path user_data_dir() override;
  };

  class Video : public IVideo {
      cvar::IntVar c_width = -1;
      cvar::IntVar c_height = -1;
      cvar::IntVar c_window_mode = 0;
      cvar::IntVar c_renderer;

      bool m_initialized {};
      SDL_GLContext m_glcontext {};
      SDL_Window* m_window {};

      bool m_change_mode(const VideoMode&);
      bool m_init_mode(const VideoMode&);

  public:
      Video();
      ~Video() override;

      bool set_mode() override;

      bool set_mode(VideoMode) override;

      void begin_frame() override;

      void end_frame() override;
  };
}

#endif //__PRIVATE__26327343
