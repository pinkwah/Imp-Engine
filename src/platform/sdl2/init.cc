#include <core/init.hh>
#include "private.hh"

using namespace imp;
namespace priv = imp::sdl2;

void imp_init_platform_sdl2()
{
    g_platform = std::make_unique<priv::Platform>();
    g_video = std::make_unique<priv::Video>();
}
