#pragma once

#include <cstddef>

namespace ix::math {
  template <class T, size_t P, size_t Q>
  class basic_fixed {
  public:
      static_assert(sizeof(T) * 8 < P + Q, "T is too small");

      using underlying_type = T;

  private:
      T m_value;

      static constexpr T m_epsilon = 1;
      static constexpr T m_unit = m_epsilon << Q;

  public:
      constexpr basic_fixed(basic_fixed)

      T int_part() const
      {}
  };

  template <class T, size_t Q>
  class basic_fixed<T, 0, Q> {
  };

  template <class T, size_t P>
  class basic_fixed<T, P, 0> {
  };

  using fixed_t = basic_fixed<int, 16, 16>;
  using angle_t = basic_fixed<int, 0, 32>;
}
