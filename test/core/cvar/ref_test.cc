#include <gtest/gtest.h>
#include <core/cvar/var.hh>
#include <core/cvar/ref.hh>

using namespace imp;

TEST(CvarRef, default_init)
{
    cvar::IntRef ref;

    ASSERT_FALSE(ref.is_valid());
}

TEST(CvarRef, init_from_var)
{
    cvar::IntVar var = 1;
    cvar::IntRef ref = var;

    ASSERT_EQ(ref.get(), 1);
}

TEST(CvarRef, reset)
{
    cvar::IntRef ref;

    ASSERT_FALSE(ref.is_valid());

    cvar::IntVar var;
    ref.reset(var);

    ASSERT_TRUE(ref.is_valid());
}

TEST(CvarRef, var_out_of_scope)
{
    cvar::IntRef ref;

    {
        cvar::IntVar var;
        ref.reset(var);

        ASSERT_TRUE(ref.is_valid());
    }

    ASSERT_FALSE(ref.is_valid());
}

TEST(CvarVar, callback_assign_var)
{
    bool callback_called {};

    cvar::IntVar var = 1;
    cvar::IntRef ref = var;
    var.set_callback([&](const int& new_value) {
        ASSERT_EQ(new_value, 2);
        callback_called = true;
    });

    ref = 2;
    ASSERT_TRUE(callback_called);
}

TEST(CvarRef, callback_assign_ref)
{
    bool callback_called {};

    cvar::IntVar var = 1;
    cvar::IntRef ref = var;
    ref.set_callback([&](const int& new_value) {
        ASSERT_EQ(new_value, 2);
        callback_called = true;
    });

    var = 2;
    ASSERT_TRUE(callback_called);
}

TEST(CvarRef, str_init)
{
    cvar::StrVar var { "helo" };
    cvar::StrRef ref = var;

    ASSERT_EQ(ref.get(), "helo");
}

TEST(CvarRef, str_assign)
{
    cvar::StrVar var { "helo" };
    cvar::StrRef ref = var;

    ref = "world";
    ASSERT_EQ(ref.get(), "world");
    ASSERT_EQ(var.get(), "world");
}

TEST(CvarRef, type_erased_assign)
{
    cvar::IntVar var = 1;
    cvar::Ref ref = var;

    ref = "10";
    ASSERT_EQ(var.get(), 10);
    ASSERT_EQ(ref.get(), "10");
}

TEST(CvarRef, type_erased_set_to_default)
{
    cvar::IntVar var = 1;
    cvar::Ref ref = var;

    ref = "10";
    ref.set_to_default();

    ASSERT_EQ(var.get(), 1);
    ASSERT_EQ(ref.get(), "1");
}

#if IMP_DEBUG
TEST(CvarRef, dbg_no_value_access)
{
    cvar::IntRef ref;

    ASSERT_THROW(ref.get(), cvar::ref_error);
}

TEST(CvarRef, dbg_no_value_assign)
{
    cvar::IntRef ref;

    ASSERT_THROW(ref = 1, cvar::ref_error);
}

TEST(CvarRef, dbg_no_value_arithmetic)
{
    cvar::IntRef ref;

    ASSERT_THROW(ref += 1, cvar::ref_error);
    ASSERT_THROW(ref -= 1, cvar::ref_error);
    ASSERT_THROW(ref *= 1, cvar::ref_error);
    ASSERT_THROW(ref %= 1, cvar::ref_error);
    ASSERT_THROW(ref /= 1, cvar::ref_error);
}
#endif
