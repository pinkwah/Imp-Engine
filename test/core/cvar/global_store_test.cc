#include <gtest/gtest.h>
#include <core/cvar.hh>

using namespace imp;

class GlobalStore : public ::testing::Test {
protected:
    void SetUp() override
    { cvar::g_store.reset(new cvar::Store); }

    void TearDown() override
    { cvar::g_store = nullptr; }
};

TEST_F(GlobalStore, add_var)
{
    cvar::IntVar var = 1;
    cvar::g_store->add(var, "test", "Test Var");

    auto ref = *cvar::g_store->find<int64>("test");
    ASSERT_EQ(ref.name(), "test");
    ASSERT_EQ(ref.desc(), "Test Var");
    ASSERT_EQ(ref.get(), 1);
}

TEST_F(GlobalStore, ref_string_ctor)
{
    cvar::IntVar var = 1;
    cvar::g_store->add(var, "test", "Test Var");

    cvar::IntRef ref("test");
    ASSERT_EQ(ref.name(), "test");
    ASSERT_EQ(ref.desc(), "Test Var");
    ASSERT_EQ(ref.get(), 1);
}

TEST_F(GlobalStore, helper)
{
    cvar::IntVar var1 = 1;
    cvar::IntVar var2 = 2;
    cvar::IntVar var3 = 3;

    cvar::Register()
        (var1, "var1", "Var One")
        (var2, "var2", "Var Two")
        (var3, "var3", "Var Three");

    cvar::IntRef ref1("var1");
    cvar::IntRef ref2("var2");
    cvar::IntRef ref3("var3");

    ASSERT_EQ(ref1.name(), "var1");
    ASSERT_EQ(ref1.desc(), "Var One");
    ASSERT_EQ(ref1.get(), 1);

    ASSERT_EQ(ref2.name(), "var2");
    ASSERT_EQ(ref2.desc(), "Var Two");
    ASSERT_EQ(ref2.get(), 2);

    ASSERT_EQ(ref3.name(), "var3");
    ASSERT_EQ(ref3.desc(), "Var Three");
    ASSERT_EQ(ref3.get(), 3);
}

TEST_F(GlobalStore, helper_with_flag)
{
    cvar::IntVar var1 = 1;
    cvar::IntVar var2 = 2;
    cvar::IntVar var3 = 3;

    cvar::Register()
        .set_flag(cvar::Flag::config)
        (var1, "var1", "Var One")
        (var2, "var2", "Var Two")
        .reset_flag(cvar::Flag::config)
        (var3, "var3", "Var Three");

    cvar::IntRef ref1("var1");
    cvar::IntRef ref2("var2");
    cvar::IntRef ref3("var3");

    ASSERT_EQ(ref1.name(), "var1");
    ASSERT_EQ(ref1.desc(), "Var One");
    ASSERT_EQ(ref1.get(), 1);
    ASSERT_TRUE(ref1.is_config());

    ASSERT_EQ(ref2.name(), "var2");
    ASSERT_EQ(ref2.desc(), "Var Two");
    ASSERT_EQ(ref2.get(), 2);
    ASSERT_TRUE(ref2.is_config());

    ASSERT_EQ(ref3.name(), "var3");
    ASSERT_EQ(ref3.desc(), "Var Three");
    ASSERT_EQ(ref3.get(), 3);
    ASSERT_FALSE(ref3.is_config());
}

TEST_F(GlobalStore, helper_with_flags)
{
    cvar::IntVar var1 = 1;
    cvar::IntVar var2 = 2;
    cvar::IntVar var3 = 3;

    cvar::Register({ cvar::Flag::config })
        (var1, "var1", "Var One")
        .reset_flags({ cvar::Flag::config })
        (var2, "var2", "Var Two")
        .set_flags({ cvar::Flag::config })
        (var3, "var3", "Var Three");

    cvar::IntRef ref1("var1");
    cvar::IntRef ref2("var2");
    cvar::IntRef ref3("var3");

    ASSERT_EQ(ref1.name(), "var1");
    ASSERT_EQ(ref1.desc(), "Var One");
    ASSERT_EQ(ref1.get(), 1);
    ASSERT_TRUE(ref1.is_config());

    ASSERT_EQ(ref2.name(), "var2");
    ASSERT_EQ(ref2.desc(), "Var Two");
    ASSERT_EQ(ref2.get(), 2);
    ASSERT_FALSE(ref2.is_config());

    ASSERT_EQ(ref3.name(), "var3");
    ASSERT_EQ(ref3.desc(), "Var Three");
    ASSERT_EQ(ref3.get(), 3);
    ASSERT_TRUE(ref3.is_config());
}
